import { eUser } from "~/Components/Model/eUser";
import { setCookie } from "~/Utils/Cookies";

/* const baseUrl = '';
const prevendUrl = 'https://teachingserver.onrender.com/'; */

export async function fetchAllMovies(callback: any) {
    const url = 'https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/nowAndSoon';
    const controller = new AbortController();
    const timeoutId = setTimeout(() => controller.abort(), 3000);

    try {
        const response = await fetch(url, {
            signal: controller.signal
        })
        return await response.json();
    }
    catch (error) {
        console.error(error);
        callback();
    }
    finally {
        clearTimeout(timeoutId);
    }
}

export async function fetchAllCinemas() {
    let res = await fetch('https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/cinemas')
    let data = await res.json();
    return data
}

export async function fetchShowtime(id: string) {
    let res = await fetch(`https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/movie/${id}`)
    let data = await res.json();
    return data
}

export async function fecthAllBooking() {
    let reg = await fetch('https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/booking/detail')
    let data = await reg.json();
    return data;
}

export async function fetchAllMoviesFromCinema(id: string) {
    let res = await fetch(`https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/cinemas/${id}`)
    let data = await res.json();
    return data
}

export async function fetchBookingTicket(payload: any) {
    let resp = await fetch('https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/Ticket', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'accept': 'application/json'
        },
        body: JSON.stringify(payload.data)
    })

    if (resp.status === 200) {
        payload.callback(true);
    }
}

export async function fetchCreateBankCard(payload: eUser) {
    const data = {
        "BankId": payload.bankId,
        "CardNumber": payload.cardNumber,
        "CardName": payload.name,
        "ExpireDate": payload.expDate,
        "CVV": payload.cvv
    }

    const url = 'https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/Bank/BankCard'
    let resp = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'accept': 'application/json'
        },
        body: JSON.stringify(data)
    })

    let res = await fetch('https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/Bank/BankCard', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'accept': 'application/json'
        },
        body: JSON.stringify({ ...data, "Balance": 20000000 })
    })


    if (resp.status === 200 && res.status === 200)
        alert('Success!')
}
export async function fetchUser(username: string, password: string) {
    let resp = await fetch('https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/user/Login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'accept': 'application/json'
        },
        body: JSON.stringify({
            "Email": username,
            "Password": password
        })
    })
    let data = await resp.json();
    if (resp.ok) {
        const status = { name: data.Name, email: data.Email }
        setCookie('user', status, 7);
    }
    return data;
}

export async function fetchRegister(user: any, callback: any) {
    const url = 'https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/user/user';
    const resp = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'accept': 'application/json'
        },
        body: JSON.stringify({
            "Email": user.email,
            "Name": user.name,
            "Password": user.password,
            "Role": "0"
        })
    })
    if (resp.status === 200) {
        alert('Bạn đã đăng ký thành công!')
        const status = { name: user.name, email: user.email }
        setCookie('user', status, 7);
        callback('/')
        return true;
    }
    return false;
}

export async function fetchUpdateUserInfo(payload: Array<string>) {
    const url = 'https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/user/user';
    let resp = await fetch(url, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            accept: 'application/json',
        },
        body: JSON.stringify({
            Email: payload[0],
            Name: payload[1],
            Password: payload[2],
        }),
    });
    return resp.status;
}

export async function fetchUpdateUserPassword(payload: Array<string>) {
    let resp = await fetch(
        'https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/user/ChangePassword',
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                accept: 'application/json',
            },
            body: JSON.stringify({
                "Email": payload[0],
                "Password": payload[1],
                "PasswordNew": payload[2]
            }),
        }
    );
    return resp.status;
}
