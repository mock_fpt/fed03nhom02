import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'

import AllMovies from "./Reducers/Movies";
import AllCinemas from './Reducers/Cinemas';
import AllBooking from './Reducers/BookingDetail'
import UserReducer from './Reducers/User'
import mySaga from "./mySaga";

const allReducers = combineReducers({
    AllMovies,
    AllCinemas,
    AllBooking,
    UserReducer
})

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()

// mount it on the Store
const store = createStore(
    allReducers,
    applyMiddleware(sagaMiddleware)
)

// then run the saga
sagaMiddleware.run(mySaga)

export default store;