import eAction from "~/Components/Model/eAction";

export type eInfo = {
    movie: {
        imgLandscape: string,
        imgPortrait: string,
        name: string,
        subName: string,
        age: string
    },
    cinema: string,
    theater: string,
    showdate: string,
    showtime: string,
}

const intialState = {
    booking: null,
    info: {
        movie: {
            imgLandscape: '',
            imgPortrait: '',
            name: '',
            subName: '',
            age: ''
        },
        cinema: '',
        theater: '',
        showdate: '',
        showtime: '',
    } as eInfo
}

const bookingDetail = (state = intialState, { payload, type }: eAction) => {
    switch (type) {
        case 'GET_BOOKING':
            return { ...state, booking: payload };

        case 'SET_INFO':
            return { ...state, info: payload }

        default:
            return state;
    }
}

export default bookingDetail;