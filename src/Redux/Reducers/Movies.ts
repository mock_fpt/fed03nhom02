import eAction from "~/Components/Model/eAction";

const intialState = {
    showingMovies : [],
    comingMovies: [],
    schedule: []
}

const AllMovies = (state = intialState, {payload, type}:eAction) => {
    switch (type) {
        case 'GET_MOVIES':            
            return {
                ...state,
                showingMovies: payload.movieShowing,
                comingMovies: payload.movieCommingSoon,
            };

        case 'GET_SHOWTIME':
            return {
                ...state,
                schedule: payload
            }

        case 'GET_MOVIES_BY_CINEMA':
            return {
                ...state, 
                showingMovies: payload,
            }
    
        default:
            return state;
    }
}

export default AllMovies