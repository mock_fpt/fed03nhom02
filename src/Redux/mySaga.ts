import { takeEvery, call, put } from "redux-saga/effects"
import { setCookie } from "~/Utils/Cookies";
import {
    fecthAllBooking,
    fetchAllCinemas,
    fetchAllMovies,
    fetchAllMoviesFromCinema,
    fetchShowtime,
    fetchUser,
    fetchBookingTicket,
    fetchCreateBankCard,
    fetchUpdateUserInfo,
    fetchUpdateUserPassword,
    fetchRegister,
} from './Fetching'

function* getBooking(): any {
    let data = yield call(fecthAllBooking);
    yield put({
        type: 'GET_BOOKING',
        payload: data
    })
}

function* getCinemas(): any {
    let data = yield call(fetchAllCinemas);
    yield put({
        type: 'GET_CINEMAS',
        payload: data
    })
}

function* getData({ payload }: any): any {
    let data = yield call(fetchAllMovies, payload);
    if (data) {
        yield put({ type: 'GET_MOVIES', payload: data, })
    }

}

function* getMoviesFromCinema({ payload }: any): any {
    let data = yield call(fetchAllMoviesFromCinema, payload);
    yield put({
        type: 'GET_MOVIES_BY_CINEMA',
        payload: data,
    })
}

function* getShowtime({ payload }: any): any {
    let data = yield call(fetchShowtime, payload);
    yield put({
        type: 'GET_SHOWTIME',
        payload: data,
    })
}

function* handleLogin({ payload }: any): any {
    let data = yield call(fetchUser, payload.username, payload.password);
    yield put({ type: 'SET_USER', payload: { name: data.Name, email: data.Email } })
}

function* handleRegister({ payload }: any): any {
    let data = yield call(fetchRegister, payload.user, payload.callback)
    if (data)
        yield put({
            type: 'SET_USER',
            payload: {
                name: payload.user.name,
                email: payload.user.email
            }
        })
    else alert('Đăng ký thất bại, vui lòng thử lại!')
}

function* setBooking({ payload }: any) {
    yield put({ type: 'SET_INFO', payload })
}

function* bookTicket({ payload }: any): any {
    yield call(fetchBookingTicket, payload);
}

function* updateUserInfo({ payload }: any): any {
    const { data, setSuccessMsg, setErrorMsg } = payload;
    let respon = yield call(fetchUpdateUserInfo, data);
    if (respon === 200) {
        const user = { name: data[1], email: data[0] };
        setCookie('user', user, 7);
        yield put({ type: 'SET_USER', payload: user })
        setSuccessMsg(true)
    }
    else setErrorMsg(true);
}

function* updateUserPassword({ payload }: any): any {
    const { data, setSuccessMsg, setErrorMsg } = payload;
    let respon = yield call(fetchUpdateUserPassword, data);
    respon === 200 ? setSuccessMsg(true) : setErrorMsg(true);
}

function* createBankCard({ payload }: any) {
    yield call(fetchCreateBankCard, payload)
}

function* mySaga() {
    yield takeEvery("GET_ALL", getData);
    yield takeEvery("GET_SCHEDULE", getShowtime);
    yield takeEvery("GET_CINEMA", getCinemas);
    yield takeEvery('GET_ALL_MOVIES_FROM_CINEMA', getMoviesFromCinema);
    yield takeEvery("GET_Booking", getBooking);
    yield takeEvery('LOGIN', handleLogin);
    yield takeEvery('UPDATE_USER', updateUserInfo);
    yield takeEvery('UPDATE_PASSWORD', updateUserPassword);
    yield takeEvery('POST_TICKETS', bookTicket);
    yield takeEvery('SET_BOOKING', setBooking);
    yield takeEvery('CREATE_BANKCARD', createBankCard);
    yield takeEvery('REGISTER', handleRegister);
}

export default mySaga;