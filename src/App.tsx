import { Route, Routes } from 'react-router-dom'
import { useDispatch} from 'react-redux';
import { useEffect } from 'react'

import Header from './Components/Default/Header';
import Footer from './Components/Default/Footer/Footer';
import Home from './Components/Home';
import MovieDetails from './Components/Movie/MovieDetails';
import Showing from './Components/AllMovies';
import Cinemas from './Components/Cinemas';
import Cinema from './Components/Cinemas/Cinema';
import PageNotFound from './Components/PageNotFound';
import Booking from './Components/BookingDetail';
import UserAuth from './Components/User';
import LoginAuth from './Components/Login';
import LoadHome from './Components/loading/loadHome/loadHome';
import Success from './Components/BookingDetail/Success';
import { getCookie } from './Utils/Cookies';
import Term from './Components/BookingDetail/Terms';
import Maintain from './Components/PageNotFound/Maintain';


function App() {
	const dispatch = useDispatch()

	useEffect(() => {
		const user = (getCookie('user'));
		user && dispatch({type:'SET_USER',payload: JSON.parse(user)}) 
	}, [dispatch])

	return (
		<div className="App">
			<Header />
			<Routes>
				<Route path='' element={<Home />} />
				<Route path='/login' element={<LoginAuth />} />
				<Route path='/user' element={<UserAuth />} />
				<Route path='/cinema' element={<Cinemas />} />
				<Route path='/cinema/:slug' element={<Cinema />} />
				<Route path='/all-movies' element={<Showing />} />
				<Route path='/detail/:slug' element={<MovieDetails />} />
				<Route path='/booking/:id' element={<Booking />} />
				<Route path='/booking/:id/success' element={<Success />} />
				<Route path='/loading' element={<LoadHome />} />
				<Route path='/terms' element={<Term />} />
				<Route path='/maintenance' element={<Maintain />} />		
				<Route path='/*' element={<PageNotFound />} />
			</Routes>
			<Footer />
		</div>
	);
}

export default App;
