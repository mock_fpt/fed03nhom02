import { Component } from "react";
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useNavigate } from "react-router-dom";

import eMovie from "~/Components/Model/eMovie";

function Background({ bgUrl, slug }: { bgUrl: string, slug: string }) {
    const style = {
        backgroundImage: `url(${bgUrl})`,
        backgroundRepeat: 'no-repeat'
    } as React.CSSProperties;

    const navigate = useNavigate();

    return <div
        className="banner-img"
        style={style}
        onClick={() => { navigate(`/detail/${slug}`) }}
    />
}

export default class Banner extends Component<{ movielist: Array<eMovie> }>  {
    render() {
        const movielist: Array<eMovie> = this.props.movielist;
        const settings = {
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2500
        };

        return (
            <div className="banner">
                <Slider {...settings}>
                    {
                        movielist.map((item, index) => {
                            return <Background bgUrl={item.imageLandscapeMobile} slug={item.slug} key={index} />
                        })
                    }
                </Slider>
            </div>
        );
    }
}

