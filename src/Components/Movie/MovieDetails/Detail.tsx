import React from 'react'
import eMovie from '~/Components/Model/eMovie'
import { toTime } from '~/Utils/Timing'

const Detail = ({ movie }: { movie: eMovie }) => {
    return (
        <div className='px_2'>
            <ul className='color_gray m_4-b'>
                <li>
                    <span className='star'></span>
                    &nbsp; {movie.point.toFixed(1)} / 10
                </li>
                <li>
                    {parseInt(movie.age) > 0 && (<span className={`age age-${movie.age} m_3-r`}>C{movie.age}</span>)}
                    <span className='clock-left'></span>
                    &nbsp; {movie.duration ?? '?'} phút
                </li>
                <li>
                    Ngày khởi chiếu: {toTime(movie.startdate)}
                </li>
            </ul>
            <div>
                <h3 className='py_1 plot'>NỘI DUNG PHIM</h3>
                <div dangerouslySetInnerHTML={{ __html: movie.description }} className='text-justify summary' />
            </div>
        </div>
    )
}

export default Detail