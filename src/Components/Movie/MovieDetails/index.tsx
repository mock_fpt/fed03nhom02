import { useNavigate, useParams } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import './Detail.scss'

import eMovie from '~/Components/Model/eMovie';
import MovieCardLandscape from '../MovieCardLanscape';
import Detail from './Detail';
import Schedule from '../Schedule/Schedule';

export default function MovieDetails() {
    const [movie, setMovie] = useState<eMovie>();
    const [showTrailer, setShowTrailer] = useState(false);
    const { slug } = useParams();
    const navigate = useNavigate()
    const dispatch = useDispatch();

    const closeTrailer = () => {
        if (movie) {
            setShowTrailer(false);
            const video = (document.getElementById('youtube-trailer') as HTMLIFrameElement);
            video?.setAttribute('src', '')
            video?.setAttribute('src', movie.trailer.replace('watch?v=', 'embed/'))
        }
    }

    /* get all movies from database */
    const allMovies = useSelector((state: any) => {
        return state.AllMovies
    });

    useEffect(() => {
        dispatch({ type: 'GET_ALL' })
    }, [dispatch]);

    /* set movie by find movies from allMovies */
    useEffect(() => {
        let result = allMovies.showingMovies.find((m: eMovie) => m.slug === slug) || allMovies.comingMovies.find((m: eMovie) => m.slug === slug)
        setMovie(result);
    }, [slug, allMovies])

    /* render */
    return (
        <div id='detail'>
            {
                movie && (
                    <>
                        <div className='py_5 main_width m_auto'>
                            <div>
                                <div className='font-lg title'>
                                    <h2>{movie.name}</h2>
                                    <h3 className='color_gray m_1-t'>{movie.subName}</h3>
                                </div>
                                <div className='brief'>
                                    <div
                                        onClick={() => setShowTrailer(true)}
                                        className='illustraction'
                                    >
                                        <img src={movie.imagePortrait} alt="illustration" className='w_100 rounded-sm' />
                                        <div className='proHover'>
                                            <h3 className='gopro'>Xem Trailer</h3>
                                        </div>
                                    </div>
                                    <Detail movie={movie} />
                                </div>
                            </div>
                            <div className='showing px_1'>
                                <h2> Phim đang chiếu</h2>
                                {
                                    allMovies.showingMovies.slice(0, 2).map((item: eMovie) =>
                                        <MovieCardLandscape movie={item} key={item.id} />
                                    )
                                }
                                <button
                                    id='more'
                                    className='w_50 right'
                                    onClick={() => navigate('/all-movies')}
                                >
                                    Xem thêm
                                </button>
                            </div>
                            
                        </div>
                        <Schedule movie={movie} />
                        <div
                            className={`trailer ${!showTrailer ? 'd_none' : 'd_block'}`}
                            onClick={() => setShowTrailer(false)}
                        >
                            <div className='rounded-md' onClick={(event) => event.stopPropagation()}>
                                <div className="d_flex border-b">
                                    <h3 className='p_2-t inline flex-grow-1 text-center color_black'>
                                        TRAILER PHIM {movie.name.toUpperCase()}
                                    </h3>
                                    <button className='close-btn' onClick={closeTrailer}>
                                        <FontAwesomeIcon icon={faTimes} className='icon-24' />
                                    </button>
                                </div>
                                <div className='p_1'>
                                    <iframe
                                        id='youtube-trailer'
                                        title='trailer'
                                        src={movie?.trailer?.replace('watch?v=', 'embed/')}
                                    >
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </>
                )
            }
        </div>
    )
}
