import { useNavigate } from "react-router-dom"
import eMovie from "../Model/eMovie";
import './MovieCard.scss'

export default function MovieCard({ movie }: { movie: eMovie }) {
    const navigate = useNavigate();
    return (
        <div className="movie-card">
            <img
                alt="illustration"
                src={movie.imagePortrait}
                onClick={() => navigate(`/detail/${movie.slug}`)}
            />
            <div>
                <h3 className="color_white">{movie.name}</h3>
                <p className="color_gray">{movie.subName}</p>
            </div>
        </div>
    )
};
