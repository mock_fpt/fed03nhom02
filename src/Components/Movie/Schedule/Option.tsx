import React, { useState } from 'react'
import eBundle from '~/Components/Model/eBundle'
import eCinema from '~/Components/Model/eCinema'
import eDate from '~/Components/Model/eDate'
import eSession from '~/Components/Model/eSession'

interface eProps {
    callback: any,
    cinemas: Array<eCinema>
}

const Option = ({ callback, cinemas }: eProps) => {
    const [index, setIndex] = useState<number>(0);
    
    return (
        <>
            <div>
                <ul className='showdate'>
                    {
                        cinemas && cinemas[0]?.dates.map((d: eDate, i: number) =>
                            <li
                                key={i}
                                onClick={() => setIndex(i)}
                                className={`${index === i ? 'active' : ''}`}
                            >
                                <p>{d.dayOfWeekLabel}</p>
                                <p>{d.showDate.slice(0, 5)}</p>
                            </li>
                        )
                    }
                </ul>
            </div>
            {
                cinemas.map((theater: eCinema) =>
                    <div className="my_4" key={theater.id}>
                        {
                            theater.dates[index]?.bundles.length > 0 && <>
                                <div className="movie-schedule-date">
                                    <h3 className="theater-name p_1 color_white">{theater.name}</h3>
                                </div>
                                <div className="border">
                                    {
                                        theater.dates[index]?.bundles.map((b: eBundle) =>
                                            <div key={b.code} className='d_flex gap-2'>
                                                <div className='theater__img'>
                                                    <img alt='' src={theater.imageUrls[1]} className='w_100' />
                                                </div>
                                                <div className='d_flex flex-grow-1 p_2'>
                                                    <div className="w_25">
                                                        <p>
                                                            {b.version.toLocaleUpperCase()}
                                                            {' - '}
                                                            {b.caption === 'sub' ? 'Phụ đề' : 'Lồng tiếng'}
                                                        </p>
                                                    </div>
                                                    <div className="w_75 d_flex flex_wrap">
                                                        {
                                                            b.sessions.map((sess: eSession) =>
                                                                <span
                                                                    className="time m_auto"
                                                                    key={sess.id}
                                                                    onClick={() => callback(theater, sess)}
                                                                >
                                                                    {sess.showTime}
                                                                </span>
                                                            )
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    }
                                </div>
                            </>
                        }
                    </div>
                )}
        </>
    )
}

export default Option