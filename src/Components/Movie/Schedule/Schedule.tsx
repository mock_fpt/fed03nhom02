import { useEffect, useState, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import './Schedule.scss'
import eSession from "../../Model/eSession";
import eDate from "../../Model/eDate";
import eMovie from "../../Model/eMovie";
import eCinema, { nullCinema } from "~/Components/Model/eCinema";
import eCity, { nullCity } from "~/Components/Model/eCity";
import { useNavigate } from "react-router-dom";
// import LoadHome from "~/Components/loading/loadHome/loadHome";
import Option from "./Option";

/* const ShowLoading = () => {
    return <div className="loading-showtime">
        <div><LoadHome /></div>
        <div className="border">
            <div><LoadHome /></div>
            <div><LoadHome /></div>
        </div>
    </div>
} */

export default function Schedule({ movie }: { movie: eMovie }) {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const temp = useSelector((state: any) => state.AllMovies.schedule);
    const [showtime, setShowtime] = useState<Array<any>>([])
    const [city, setCity] = useState<eCity>(nullCity)
    const [cinema, setCinema] = useState<eCinema>(nullCinema)
    const [provinces, setProvinces] = useState<Array<eCity>>([]);
    const [collapse, setCollapse] = useState<Array<boolean>>([false, false, false]);

    const directToBook = useCallback((t: any, s: eSession) => {
        const sd = t.dates.find((d: eDate) => d.showDate === s.showDate);
        const data = {
            movie: {
                imgLandscape: movie.imageLandscape,
                imgPortrait: movie.imagePortrait,
                name: movie.name,
                subName: movie.subName,
                age: movie.age
            },
            cinema: `${t.name}`,
            theater: `${s.screenName}`,
            showdate: `${sd.dayOfWeekLabel}, ${sd.showDate}`,
            showtime: s.showTime,
        }
        dispatch({ type: 'SET_BOOKING', payload: data })
        navigate(`/booking/${s.id}?mid=${movie.id}`)
    }, [movie, dispatch, navigate])

    useEffect(() => {
        dispatch({ type: 'GET_SCHEDULE', payload: movie.id })
    }, [dispatch, movie]);

    useEffect(() => {
        fetch('https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/city')
            .then(resp => resp.json())
            .then(data => setProvinces(data))
    }, [])

    useEffect(() => { setShowtime(temp) }, [temp])

    const filterShowtime = useCallback(() => {
        let result = showtime;
        if (city.id !== '')
            result = showtime.filter((t: eCinema) => t.cityId === city.id);
        if (cinema.id !== '')
            result = showtime.filter((t: eCinema) => t.id === cinema.id);
        return result;
    }, [cinema, city, showtime])

    return (
        <div className="movie-schedule main_width m_auto">
            <div className="d_flex jus_space my_2">
                <h3 className="font-xl title">Lịch chiếu</h3>
                <div className="w_75 d_flex jus_end gap-2">
                    <summary
                        className="select w_33"
                        onClick={() => setCollapse(collapse.map((c: boolean, i: number) => i === 0 ? !c : c))}
                        onMouseLeave={() => setCollapse(collapse.map((c: boolean, i: number) => i === 0 ? false : c))}
                    >
                        <span>{city.name ? city.name : 'Cả nước'}</span>
                        <ul className={`${collapse[0] ? 'visible' : 'hidden'} border-t`}>
                            <li onClick={() => setCity(nullCity)}>Cả nước</li>
                            {
                                provinces.map((city: eCity) =>
                                    <li key={city.slug} onClick={() => setCity(city)}>
                                        {city.name}
                                    </li>
                                )}
                        </ul>
                    </summary>
                    <summary
                        className="select w_33"
                        onClick={() => setCollapse(collapse.map((c: boolean, i: number) => i === 1 ? !c : c))}
                        onMouseLeave={() => setCollapse(collapse.map((c: boolean, i: number) => i === 1 ? false : c))}
                    >
                        <span>{cinema.name ? cinema.name : 'Tất cả các rạp'}</span>
                        <ul className={`${collapse[1] ? 'visible' : 'hidden'} border-t`}>
                            <li onClick={() => setCinema(nullCinema)}>Tất cả các rạp</li>
                            {
                                filterShowtime().map((t: eCinema) =>
                                    <li key={t.name} onClick={() => setCinema(t)}>
                                        {t.name}
                                    </li>
                                )}
                        </ul>
                    </summary>
                </div>
            </div>
            <>
                {
                    showtime.length > 0 && 
                        <Option callback={directToBook} cinemas={filterShowtime()} />
                }
            </>
        </div >
    )
};

/*

*/