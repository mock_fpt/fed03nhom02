import { useNavigate, useParams } from "react-router-dom"
import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Movies from "./Movies";
import eCinema from "../Model/eCinema";
import CinemaPictures from "./CinemaPictures";

const Cinema = () => {
    const { slug } = useParams();
    let [cinema, setCinema] = useState<eCinema>();
    let [currentDate, setCurrentDate] = useState<string>((): string => {
        let time = new Date();
        return time.toISOString().slice(0, 10);
    });

    const [option, setOption] = useState<number>(0);

    const allCinemas = useSelector((state: any) => state.AllCinemas.cinemas);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        dispatch({ type: 'GET_CINEMA' })
    }, [dispatch]);

    useEffect(() => {
        let result = allCinemas.find((cinema: eCinema) => cinema.slug === slug)
        setCinema(result);
    }, [slug, allCinemas])

    const handleSearchDate = (event: any) => {
        setCurrentDate(event?.target.value);;
    }

    const directTo = (event: any) => {
        navigate(`/cinema/${event.target.value}`)
    }

    return (
        <div className="backCiCont">
            <div className="main_width m_auto" id='cinema-detail'>
                {
                    cinema && (
                        <div className="py_5">
                            <h1>{cinema.name}</h1>
                            <CinemaPictures imageUrls={cinema.imageUrls} />
                            <div className="d_flex m_4-t">
                                <div className="w_50">
                                    <div>
                                        <h3 className="py_1 title">Showing Movies</h3>
                                        <div className="d_flex jus_space p_3-t">
                                            <input type="date" value={currentDate} onChange={handleSearchDate} className='w_33' />
                                            <select name="cinema-filter" className="w_33" onChange={directTo}>
                                                {
                                                    allCinemas.map((cinema: eCinema) =>
                                                        <option
                                                            value={cinema.slug}
                                                            key={cinema.id}
                                                            className='p_1'
                                                        >
                                                            {cinema.name}
                                                        </option>
                                                    )}
                                            </select>
                                        </div>
                                        <Movies props={{ cinema: { code: cinema.code, name: cinema.name }, date: currentDate }} />
                                    </div>
                                </div>
                                <div className="w_50 m_2-l more_infor">
                                    <nav className="d_flex">
                                        <h3
                                            className="title py_1 m_1-r"
                                            onClick={() => setOption(0)}
                                        >Giá vé</h3>
                                        <h3
                                            className="title py_1"
                                            onClick={() => setOption(1)}
                                        >
                                            Thông tin thêm
                                        </h3>
                                    </nav>
                                    {
                                        option === 0 && <div className="m_3-t">
                                            <img alt='fare' src={cinema.ticket[0].url} className='w_100' />
                                        </div>
                                    }
                                    {
                                        option === 1 && <div className="m_3-t">
                                            <div className="my_2 px_1 summary">
                                                <div dangerouslySetInnerHTML={{ __html: cinema.description }} className='text-justify' />
                                                <p>
                                                    <span className="color_gray">Địa chỉ: </span>
                                                    {cinema.address}
                                                </p>
                                                <p>
                                                    <span className="color_gray">Phone number: </span>
                                                    {cinema.phone}
                                                </p>
                                            </div>
                                            <iframe src={cinema.mapEmbeb} title='address' className='address' />
                                        </div>
                                    }

                                </div>
                            </div>
                        </div >
                    )
                }
            </div >
        </div>
    )
}

export default Cinema