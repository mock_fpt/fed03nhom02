import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Arrow = () => <div style={{ display: "none" }} />;

export default class CinemaPictures extends Component<{ imageUrls: Array<string> }> {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            cssEase: "linear",
            nextArrow: <Arrow />,
            prevArrow: <Arrow />
        };
        return (
            <div className="py_4">
                <Slider {...settings}>
                    {this.props.imageUrls.map((imgurl: string, i: number) =>
                        <img alt="" src={imgurl} className='px_1' key={i} />
                    )}
                </Slider>
            </div>
        );
    }
}