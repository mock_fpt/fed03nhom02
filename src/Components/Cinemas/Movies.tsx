import { useDispatch, useSelector } from "react-redux";
import { useEffect } from 'react'
import eMovie from "../Model/eMovie";
import eDate from "../Model/eDate";
import MovieInDay from "./MovieInDay";
import { Link } from "react-router-dom";

const Movies = ({ props }: any) => {
    const options: any = {
        month: "2-digit",
        day: "2-digit",
        year: "numeric"
    };
    const {cinema} = props;
    const date = new Date(props.date).toLocaleDateString("en-GB", options)
    const dispatch = useDispatch();
    const allMovies: Array<eMovie> = useSelector((state: any) => state.AllMovies.showingMovies);

    useEffect(() => {
        dispatch({
            type: 'GET_ALL_MOVIES_FROM_CINEMA',
            payload: cinema.code
        })
    }, [dispatch, cinema])

    return (
        <div className="d_flex flex-gap-1 py_3">
            {
                allMovies && allMovies
                    .filter((movie: eMovie) => {
                        let match = [];
                        if (movie.dates)
                            match = movie.dates.filter((d: eDate) => d.showDate === date)
                        return match.length ? movie : null;
                    })
                    .map((movie: eMovie) => {
                        return (
                            <div key={movie.id} className='inday-movie border-t my_1'>
                                <Link to={`/detail/${movie.slug}`}>
                                    <img src={movie.imagePortrait} alt="" className="w_100"/>
                                </Link>
                                <div className="flex-grow-1 p_1">
                                    <div className="text-center m_2-b">
                                        <h3>{movie.name}</h3>
                                        <div className="color_gray">
                                            <span className="m_2-r">{movie.startdate.slice(0, 4)}</span>
                                            <span className='clock-left'></span>
                                            <span>&nbsp; {movie.duration ?? '?'} mins</span>
                                        </div>
                                    </div>
                                    <MovieInDay movie={movie} date={date} name={cinema.name}/>
                                </div>
                            </div>
                        )
                    })
            }
        </div>
    )
};

export default Movies;
