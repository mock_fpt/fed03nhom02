import { useEffect, useState, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom';

import eCinema, { nullCinema } from '../Model/eCinema';
import eCity, { nullCity } from '../Model/eCity';
import LoadHome from '../loading/loadHome/loadHome';
import './Cinema.scss'

const ShowLoading = () => {
    return <div className="Cineload">
        <LoadHome />
    </div>
}

export default function Cinemas() {
    const allCinemas = useSelector((state: any) => {
        return state.AllCinemas.cinemas
    });
    const dispatch = useDispatch();
    const navigate = useNavigate()
    const [collapse, setCollapse] = useState<Array<boolean>>([false, false]);
    const [provinces, setProvinces] = useState<Array<eCity>>([]);
    const [city, setCity] = useState<eCity>(nullCity)
    const [cinema, setCinema] = useState<eCinema>(nullCinema)

    const directTo = (slug: string) => navigate(`/cinema/${slug}`);

    useEffect(() => { dispatch({ type: 'GET_CINEMA' }) }, [dispatch]);

    useEffect(() => {
        fetch('https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/city')
            .then(resp => resp.json())
            .then(data => setProvinces(data))
    }, [])

    const filterCinemas = useCallback(() => {
        let result = allCinemas;
        if (city.id !== '')
            result = allCinemas.filter((t: eCinema) => t.cityId === city.id);
        if (cinema.id !== '')
            result = allCinemas.filter((t: eCinema) => t.id === cinema.id);
        return result;
    }, [cinema, city, allCinemas])

    return (
        <div className='' id='cinema-system'>
            <div className='py_5 main_width m_auto'>
                <div className='d_flex'>
                    <h1 className='w_25'>Hệ thống rạp</h1>
                    <div className="w_75 d_flex jus_end gap-2">
                        <summary
                            className="select w_33"
                            onClick={() => setCollapse(collapse.map((c: boolean, i: number) => i === 0 ? !c : c))}
                            onMouseLeave={() => setCollapse(collapse.map((c: boolean, i: number) => i === 0 ? false : c))}
                        >
                            <span>{city.name ? city.name : 'Cả nước'}</span>
                            <ul className={`${collapse[0] ? 'visible' : 'hidden'} border-t`}>
                                <li onClick={() => setCity(nullCity)}>Cả nước</li>
                                {
                                    provinces.map((city: eCity) =>
                                        <li key={city.slug} onClick={() => setCity(city)}>
                                            {city.name}
                                        </li>
                                    )}
                            </ul>
                        </summary>
                        <summary
                            className="select w_33"
                            onClick={() => setCollapse(collapse.map((c: boolean, i: number) => i === 1 ? !c : c))}
                            onMouseLeave={() => setCollapse(collapse.map((c: boolean, i: number) => i === 1 ? false : c))}
                        >
                            <span>{cinema.name ? cinema.name : 'Tất cả các rạp'}</span>
                            <ul className={`${collapse[1] ? 'visible' : 'hidden'} border-t`}>
                                <li onClick={() => setCinema(nullCinema)}>Tất cả các rạp</li>
                                {
                                    filterCinemas().map((t: eCinema) =>
                                        <li key={t.name} onClick={() => setCinema(t)}>
                                            {t.name}
                                        </li>
                                    )}
                            </ul>
                        </summary>
                    </div>
                </div>
                <div className='d_flex flex-gap-2 py_3'>
                    {!allCinemas && Array(9).fill(0).map((n: number) => {
                        return <ShowLoading key={n} />
                    })}
                    {allCinemas && filterCinemas().map((cinema: eCinema) =>
                        <div key={cinema.id} className='w_33 zoom-in' onClick={() => directTo(`${cinema.slug}`)}>
                            <img src={cinema.imageUrls[0]} alt="cinema-illustraction" className='cinema-img rounded-sm' />
                            <h3 className='text-center py_1'>{cinema.name}</h3>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}
