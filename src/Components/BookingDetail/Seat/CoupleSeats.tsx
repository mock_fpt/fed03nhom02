import React from 'react'

const CoupleSeats = ({ arr, pick, picked, sold }: any) => {
    const isPicked = (id: string, letter: string) => {
        const pos = letter + id;
        if (sold.indexOf(pos) === -1)
            return picked.indexOf(pos) === -1 ? '' : 'picked';
        return 'sold';
    }

    return (
        <>
            {arr && arr.map((row: any) => {
                return <div key={row.physicalName} className='couple-seats'>
                    <p className='d_flex align_center'>{row.physicalName}</p>
                    <div className='d_flex jus_space'>
                        {
                            row.seats
                                .filter((s: any, i: number) => i % 2 === 0)
                                .map((s: any, i: number) =>
                                    <span
                                        className={`seat-couple-0 ${isPicked(s.id, row.physicalName)}`}
                                        onClick={() => { pick(row.physicalName, s.id, s.id * 1 + 1) }}
                                        key={i}
                                    >
                                        <span key={i}> {s.id} </span>
                                        <span key={i + 1}> {s.id * 1 + 1} </span>
                                    </span>
                                )
                        }
                    </div>
                    <p className='d_flex align_center'>{row.physicalName}</p>
                </div>
            })}
        </>
    )
}

export default React.memo(CoupleSeats)