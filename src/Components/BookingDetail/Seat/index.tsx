import React, { useState, useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

import './Seat.scss'
import { BookingContext } from '..';
import CoupleSeats from './CoupleSeats';
import StandarSeats from './StandarSeats';
import Payment from '../Card/Payment';

function Seat() {
    const AllBooking = useSelector((state: any) => state.AllBooking.booking);
    const { setShow, seats, setSeats, combo } = useContext(BookingContext) as any;
    const [picked, setPicked] = useState(false)
    const [couple, setCouple] = useState<Array<string>>([]);
    const [standard, setStandard] = useState<Array<string>>([]);
    const [sold, setSold] = useState<Array<string>>([]);
    const { id } = useParams();
    
    const normalSeat = AllBooking?.seatPlan?.seatLayoutData?.areas[0];
    const coupleSeat = AllBooking?.seatPlan?.seatLayoutData?.areas[1];
    
    const handlePick = (col: string, row: string, row_?: string) => {
        let newSeat = col + row;
        if (sold.indexOf(newSeat) === -1) {
            // when user selected couple seats
            if (col === 'P') { 
                let existed = couple.indexOf(newSeat);
                if (existed === -1) { 
                    // if selected seats is not existed in array, then push them into array
                    couple.length < combo['GHE DOI'] && setCouple([...couple, newSeat, col + row_])
                }
                else setCouple(couple.filter((_, i: number) => i !== existed && i !== existed + 1))
            }
            // when user selected standard seats
            else { 
                let existed = standard.indexOf(newSeat);
                if (existed === -1) { 
                    // if selected seats is not existed in array, then push it into array
                    (standard.length < combo['Ve 2D Thanh Vien'] + combo['Nguoi Lon']) && setStandard([...standard, newSeat])
                }
                else setStandard(couple.filter((_, i: number) => i !== existed && i !== existed + 1))
            }
        }
        else {
            alert('Ghế đã được đặt, vui lòng chọn ghế khác!')
        }
    }

    const handleSubmit = () => {
        setSeats([...seats, ...couple, ...standard]);
        setPicked(true);
    }

    const handleCancel = () => {
        setSeats([]);
        setCouple([]);
        setStandard([]);
        setShow(false);
    }

    useEffect(() => {
        fetch(`https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/TicketByShowCode/${id}`)
            .then(res => res.json())
            .then(data => {
                const temp = data.map((item:any)=>item.SeatCode);
                setSold(temp.join(', ').split(', '))
            })
    }, [id])

    return (
        <div className='seatMainSize'>
            {
                picked ? <Payment back={setPicked} />
                    : <>
                        <h2>Chọn ghế</h2>
                        <div className='lsSeat'>
                            <div className='LsSeat_chose'>
                                <CoupleSeats arr={coupleSeat?.rows} pick={handlePick} picked={couple} sold={sold}/>
                                <StandarSeats arr={normalSeat?.rows} pick={handlePick} picked={standard} sold={sold}/>
                            </div>
                            <div className='aboutInfor'>
                                <div className='screen'>
                                    <h3>Màn hình</h3>
                                </div>
                                <div className='aboutInfor_content'>
                                    <div>
                                        <div style={{ background: 'lightgrey' }} />
                                        <span>Ghế có thể chọn</span>
                                    </div>
                                    <div>
                                        <div style={{ background: 'aqua' }}></div>
                                        <span>Ghế đang chọn</span>
                                    </div>
                                    <div>
                                        <div style={{ background: 'lightgreen' }}></div>
                                        <span>Ghế đã chọn</span>
                                    </div>
                                    <div>
                                        <div style={{ background: 'red' }}></div>
                                        <span>Ghế đã đặt</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='p_2-t d_flex jus_space'>
                            <button className='w_33 p_1' onClick={handleCancel}>
                                Cancel
                            </button>
                            <button className='w_33 p_1' onClick={handleSubmit}>
                                Accept
                            </button>
                        </div>
                    </>
            }
        </div>
    )
}


export default React.memo(Seat);