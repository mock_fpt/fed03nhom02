import { faMinusCircle, faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useContext, useState, memo, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { FormatPrice } from '../../../Utils/DisplayPrice'
import { BookingContext } from '..'

const BookingTickets = () => {
    const [num, setNum] = useState([0, 0, 0])
    const [total, setTotal] = useState(0);
    const { booking } = useSelector((state: any) => state.AllBooking);
    const { setCost1, setCombo, combo } = useContext(BookingContext) as any;

    useEffect(() => {
        if (booking?.ticket) {
            const sum = num.reduce((sum, n, i) => sum + n * booking.ticket[i].displayPrice, 0);
            setTotal(sum);
            setCost1(sum)
        }
    }, [num, booking, setCost1])

    useEffect(() => {
        if (booking) {
            const res = [
                ...booking.ticket.map((item: any) => {
                    return { [item.name]: 0 }
                }),
                ...booking.consession[0].concessionItems.map((item: any) => {
                    return { [item.description]: 0 }
                })]
            setCombo(res.reduce(function (result: any, item: any) {
                var key = Object.keys(item)[0]; //first property: a, b, c
                result[key] = item[key];
                return result;
            }, {}))
        }
    }, [booking, setCombo])

    const transvalue = (type: string, index: number) => {
        const name = booking.ticket[index].name
        if (type === 'add') {
            setNum(num.map((c, i) => i === index ? c + 1 : c));
            setCombo({ ...combo, [name]: combo[name] + 1 })
        }
        else if (type === 'sub') {
            setNum(num.map((c, i) => i === index ? (c < 1 ? 0 : c - 1) : c))
            setCombo({ ...combo, [name]: combo[name] > 1 ? combo[name] - 1 : 0 })
        }
    }

    const handleChange = (event: any, i: number) => {
        const amount = parseInt(event.target.value);
        const distance: number = amount - num[i];
        if (distance > 0) {
            setNum(num.map((c, index) => i === index ? amount : c))
            setCombo({
                ...combo,
                [booking.ticket[i].name]: amount
            })
        }
        else {
            setNum(num.map((c, index) => i === index ? 0 : c))
            setCombo({
                ...combo,
                [booking.ticket[i].name]: amount < 1 ? amount : 0,
            })
        }
    }

    return (
        <table className='bookingTable'>
            <thead className='font-lg'>
                <tr>
                    <th className='w_50 tableLs text-left'>Loại vé</th>
                    <th className='tableLs text-center' >Số lượng</th>
                    <th className='tableLs text-center' >Giá (VNĐ)</th>
                    <th className='tableLs text-end' >Tổng (VNĐ)</th>
                </tr>
            </thead>
            <tbody>
                {
                    booking?.ticket?.map((n: any, i: number) =>
                        <tr className='lsBooking' key={i}>
                            <td className='tableLs text-left font-weight-6'>
                                <p>{n.name}</p>
                                <p>{n.description}</p>
                            </td>
                            <td className='tableLs'>
                                <div className='d_flex jus_space align_center'>
                                    <FontAwesomeIcon
                                        onClick={() => transvalue('sub', i)}
                                        className='icon-24 fIcon'
                                        icon={faMinusCircle}
                                    />
                                    <input
                                        value={num[i]}
                                        type={'number'}
                                        onChange={(e) => handleChange(e, i)}
                                    />
                                    <FontAwesomeIcon
                                        onClick={() => transvalue('add', i)}
                                        className='icon-24 fIcon'
                                        icon={faPlusCircle}
                                    />
                                </div>
                            </td>
                            <td className='tableLs text-center'>
                                <p> {FormatPrice(n.displayPrice)}</p>
                            </td>
                            <td className='tableLs text-end'>
                                {FormatPrice(n.displayPrice * num[i])}
                            </td>
                        </tr>
                    )
                }
                <tr className='border-t'>
                    <td
                        style={{ fontWeight: '600', color: 'rgb(255, 118, 69)' }}
                        className='tableLs text-left'
                        colSpan={3}
                    >
                        Tổng
                    </td>
                    <td className='tableLs text-right'>

                        {FormatPrice(total)}
                    </td>
                </tr>
            </tbody>
        </table>
    )
}

export default memo(BookingTickets)