import React, { useContext, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormatPrice } from '../../../Utils/DisplayPrice';
import { BookingContext } from '..';
import './aboutFilm.scss'
import eSession from '~/Components/Model/eSession';
import { useParams } from 'react-router-dom';
interface eProps {
    isComplete: boolean
}

interface eRap {
    address?: string,
    name?: string,
    screenName?: string,
    showDate?: string,
    showTime?: string,
    slug?: string
}

function AboutFilm({ isComplete }: eProps) {
    const { info } = useSelector((state: any) => state.AllBooking)
    const { cost1, cost2, combo, setShow, seats, movie } = useContext(BookingContext) as any;
    const dispatch = useDispatch();
    const { id } = useParams();
    const cinemaCode = id?.substring(0, id.indexOf('-'));

    const { schedule } = useSelector((state: any) => {
        return state.AllMovies;
    })

    React.useEffect(() => {
        if (schedule.length === 0 && movie) {
            dispatch({ type: 'GET_SCHEDULE', payload: movie.id })
        }
    }, [schedule, movie, dispatch])

    const cinema: eRap = useMemo(() => {
        if (schedule.length > 0) {
            const sch = schedule.find((sch: any) => sch.code === cinemaCode);
            let result: eRap = {};
            for (const date of sch.dates) {
                for (const bundle of date.bundles) {
                    const res = bundle.sessions.find((s: eSession) => s.id === id);
                    if (res) {
                        result = {
                            ...result,
                            screenName: res.screenName ?? '',
                            showDate: res.showDate ?? '',
                            showTime: res.showTime ?? '',
                        }
                        break;
                    }
                }
            }
            return { ...result, name: sch.name, slug: sch.slug, address: sch.address };
        }
        return {}
    }, [schedule, cinemaCode, id])

    return (
        <div className='aboutFilm'>

            <div>
                <div className='filmImg'>
                    <img alt='' src={movie?.imageLandscape} />
                    <div>
                        <h3 className='py_1'>{movie?.name}</h3>
                        <span className='color_gray py_1'>{movie?.subName}</span>
                        <div className='font-sm'>
                            {
                                movie?.age > 0 &&
                                <>
                                    <span className={`age age-${movie.age}`}>C{movie.age}</span>
                                    <span style={{ color: 'red' }} className='note'>
                                        (*) Phim chỉ dành cho khán giả {movie.age} tuổi trở lên
                                    </span>
                                </>
                            }
                        </div>
                    </div>
                </div>
                <div className='ticker'>
                    <p>Rạp: {cinema && `${cinema.name} | ${cinema.screenName ?? info.theater}`} </p>
                    <p>
                        Suất chiếu:
                        {cinema && `${cinema.showTime??info.showtime} | ${cinema.showDate??info.showdate}`}
                    </p>
                    <div className='d_flex'>
                        <span>Combo: </span>
                        <ul className='inline-block'>
                            {
                                combo && Object.keys(combo).filter(key => combo[key] > 0)
                                    .map((key: string, index: number) =>
                                        <li className='m_3-l font-sm' key={index}> {key}: ({combo[key]}) </li>
                                    )
                            }
                        </ul>
                    </div>
                    <p>Ghế : {seats && seats.join(', ')}</p>
                    <h3 className='py_2'>
                        Tổng : {FormatPrice(cost1 + cost2)} VNĐ
                    </h3>
                </div>
                <div>
                    {
                        !isComplete &&
                        <button
                            onClick={() => { setShow(true) }}
                            className='continue'
                        >
                            Tiếp tục ➜
                        </button>
                    }
                </div>
            </div>
        </div>
    )
}

export default AboutFilm