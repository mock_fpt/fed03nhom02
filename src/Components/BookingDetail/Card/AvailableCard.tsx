import { memo, useEffect, useState } from 'react'
import { banks, eCard, emptyCard } from '~/Components/Model/eBank';

const MasterCard = require('~/Assets/Images/mastercard.png');

const AvailableCard = ({ user, callback }: any) => {
    const [card, setCard] = useState<eCard>(emptyCard);
    const [cards, setCards] = useState<Array<eCard>>([]);

    useEffect(() => {
        fetch(`https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/Bank/CardRef/${user.email}`)
            .then(resp => resp.json())
            .then(data => setCards(data));
    }, [user.email])


    const selectCard = (value: string) => {
        if (value) {
            const pos = value.indexOf('-');
            const bankName = value.substring(0, pos - 1);
            const current = banks.find((_: any) => _.Name === bankName)
            current && setCard({
                ...current,
                CardNumber: value.substring(pos + 2)
            })
            callback('bankId', current?.Id)
            callback('cardNumber', value.substring(pos + 2))
        }
    }

    return (
        <div className='bankcard available'>
            <div className='d_flex jus_space'>
                <img alt='' width={200} src={MasterCard} />
                <div className='text-center color_black'>
                    {
                        card.Logo ? <img alt='bank' src={card.Logo} width='72px' /> :
                            <div className='bank-logo' />
                    }
                    <h4>{card.Name}</h4>
                </div>
            </div>
            <div className='payment-input'>
                <label htmlFor="">Số tài khoản</label>
                <select onChange={(e) => selectCard(e.target.value)}>
                    <option hidden>Chọn thẻ của bạn</option>
                    {
                        cards.map((c: eCard, i: number) =>
                            <option key={i} defaultValue={i === 0 ? 'true' : 'false'}>
                                {c.Name} - {c.CardNumber}
                            </option>
                        )
                    }
                </select>
                <div className='error-msg' />
            </div>
            <div className='d_flex gap-1'>
                <div className='w_50 payment-input'>
                    <label htmlFor="">Tên chủ thẻ</label>
                    <input value={user.name} readOnly />
                    <div className='error-msg'></div>
                </div>
                <div className='w_25 payment-input'>
                    <label htmlFor="">Ngày hết hạn</label>
                    <input readOnly value='****' />
                    <div className='error-msg'></div>
                </div>
                <div className='w_25 payment-input'>
                    <label htmlFor="">CVV</label>
                    <input readOnly value='***' />
                    <div className='error-msg' />
                </div>
            </div>
        </div>
    )
}

export default memo(AvailableCard)