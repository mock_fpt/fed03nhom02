import { memo, useState } from 'react'
import { useDispatch } from 'react-redux';
import { banks, eBank, emptyCard } from '~/Components/Model/eBank';
import { validate } from '~/Utils/Regex';

const MasterCard = require('~/Assets/Images/mastercard.png');

export const NotAvailable = ({ user, callback }: any) => {
    return <div className='bankcard new'>
        <div className='d_flex jus_space'>
            <img alt='' width={200} src={MasterCard} />
        </div>
        <h4>Để dùng chức năng này vui lòng liên hệ admin!</h4>
        <div className='d_flex gap-1 py-3'>
            <div className='w_50 payment-input'>
                <label htmlFor="">Tên chủ thẻ</label>
                <input
                    value={user.name}
                    readOnly
                    onChange={(e) => callback('name', e.target.value)}
                />
                <div className='error-msg'>
                    <span>{validate('Name', user.name)}</span>
                </div>
            </div>
            <div className='w_25 payment-input'>
                <label htmlFor="">Ngày hết hạn</label>
                <input value='***' readOnly />
                <div className='error-msg'></div>
            </div>
            <div className='w_25 payment-input'>
                <label htmlFor="">CVV</label>
                <input value='****' readOnly />
                <div className='error-msg'></div>
            </div>
        </div>
    </div>
}

const NewCard = ({ user, callback }: any) => {
    const [bank, setBank] = useState<eBank>(emptyCard);
    const dispatch = useDispatch()

    const selectBank = (value: string) => {
        const b = banks.find((_: any) => _.Name === value);
        if (b) {
            setBank({ ...b, CardNumber: '' })
            callback('bankId', b.Id)
        }
    }

    const createBankCard = () => {
        if (validate('Name', user.name) || validate('ExpireDate', user.expDate) || validate('CVV', user.cvv))
            alert('Một vài thông tin đã bị sai, vui lòng thử lại!')
        else
            dispatch({ type: 'CREATE_BANKCARD', payload: user })
    }

    return (
        <>
            <div className='bankcard new'>
                <div className='d_flex jus_space'>
                    <img alt='' width={200} src={MasterCard} />
                    <div className='text-center color_black'>
                        {
                            bank.Logo ? <img alt='bank' src={bank.Logo} width='72px' /> :
                                <img alt='bank' src={banks[0].Logo} width='72px' />
                        }
                        <div className='payment-input'>
                            <select onChange={(e) => selectBank(e.target.value)}>
                                {
                                    banks.map((bank: any, i: number) =>
                                        <option
                                            value={bank.Name}
                                            key={i}
                                            defaultValue={i === 0 ? 'true' : 'false'}
                                        >
                                            {bank.Name}
                                        </option>
                                    )
                                }
                            </select>
                        </div>
                    </div>
                </div>
                <div className='payment-input'>
                    <label htmlFor="">Số tài khoản</label>
                    <input
                        value={user.cardNumber}
                        onChange={(e) => callback('cardNumber', e.target.value)}
                    />
                    <div className='error-msg'>
                        <span>{validate('CardNumber', user.cardNumber)}</span>
                    </div>
                </div>
                <div className='d_flex gap-1'>
                    <div className='w_50 payment-input'>
                        <label htmlFor="">Tên chủ thẻ</label>
                        <input
                            value={user.name}
                            onChange={(e) => callback('name', e.target.value)}
                        />
                        <div className='error-msg'>
                            <span>{validate('Name', user.name)}</span>
                        </div>
                    </div>
                    <div className='w_25 payment-input'>
                        <label htmlFor="">Ngày hết hạn</label>
                        <input
                            value={user.expDate}
                            onChange={(e) => callback('expDate', e.target.value)}
                        />
                        <div className='error-msg'>
                            <span>{validate('ExpireDate', user.expDate)}</span>
                        </div>
                    </div>
                    <div className='w_25 payment-input'>
                        <label htmlFor="">CVV</label>
                        <input
                            value={user.cvv}
                            onChange={(e) => callback('cvv', e.target.value)}
                        />
                        <div className='error-msg'>
                            <span>{validate('CVV', user.cvv)}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button className='w_50 p_1 m_auto d_block' onClick={createBankCard}>
                    Nhấn vào đây để tạo thẻ mới
                </button>
            </div>
        </>
    )
}

export default memo(NewCard)