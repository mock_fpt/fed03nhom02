export default interface eSession {
    id: string,
    cinemaId: string,
    typeCode: string,
    dayOfWeekLabel: string,
    dayOfWeekKey: string,
    showDate: string,
    showTime: string,
    seatsAvailable: number,
    priceGroupCode: string,
    screenName: string,
    // screenNameAlt: string,
    // screenNumber: number,
    cinemaOperatorCode: string,
    sessionBusinessDate: string
}    