import eDate from "./eDate";

export default interface eCinema {
    id: string,
    name: string,
    slug: string,
    address: string,
    code: string,
    phone: string,
    mapEmbeb: string,
    imageUrls: Array<string>,
    dates: Array<eDate>,
    description: string,
    ticket: Array<any>,
    cityId: string
}

export const nullCinema : eCinema = {
    id: '',
    name: '',
    slug: '',
    address: '',
    code: '',
    phone: '',
    mapEmbeb: '',
    imageUrls: [],
    dates: [],
    description: '',
    ticket: [],
    cityId: ''
}