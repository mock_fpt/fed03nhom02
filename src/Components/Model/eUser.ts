export interface eUser {
    name: string,
    email: string,
    balance: number,
    bankId: number,
    expDate: string,
    cvv: string,
    cardNumber: string,
    phone: string,
}

export const unknowUser = {
    name: "",
    email: "",
    balance: 0,
    bankId: 0,
    expDate: "",
    cvv: "",
    cardNumber: "",
    phone: "",
} as eUser;