import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import './style.scss'
import { getCookie } from '~/Utils/Cookies';
import ShowingMovie from '../Movie/ShowingMovie';
import { unknowUser } from '../Model/eUser';
import History from './History';
import UserInfor from './UserInfor';

function User() {
	const allMovies = useSelector((state: any) => state.AllMovies);
	const dispatch = useDispatch();
	const [option, setOption] = useState(0)
	const [user, setUser] = useState(() => {
		const user = getCookie('user')
		return (user) ? { ...unknowUser, ...JSON.parse(user) } : unknowUser;
	})

	useEffect(() => { dispatch({ type: 'GET_ALL' }) }, [dispatch]);

	const changeUserInfo = (key: string, value: string) => { setUser({ ...user, [key]: value }) }

	return (
		<div className='user'>
			<div className='p_5-t main_width m_auto'>
				<h1>QUẢN LÝ NGƯỜI DÙNG</h1>
				<div className='d_flex my_3 gap-2'>
					<div className='dashboard'>
						<ul>
							<li 
								onClick={() => setOption(0)}
								className={`${option === 0 ? 'active' : ''}`}
							>
								Thông tin thành viên
							</li>
							<li 
								className={`${option === 1 ? 'active' : ''}`}
								onClick={() => setOption(1)}>
								Vé đã mua
							</li>
						</ul>
					</div>
					<div className='user-detail'>
						{ option === 0 && <UserInfor user={user} callback={changeUserInfo} /> }
						{ option === 1 && <History mail={user.email} /> }
					</div>
				</div>
			</div>

			<div className="py_3" style={{backgroundColor: '#121212'}}>
				<div className='main_width m_auto'>
					<div className="py_2 border-b">
						<h2 className="color_orange">Showing Movie</h2>
					</div>
					<ShowingMovie movielist={allMovies.showingMovies.slice(0, 4)} />
				</div>
			</div>
		</div>
	)
}


export default User;
