import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTicket } from '@fortawesome/free-solid-svg-icons';

const History = ({ mail }: { mail: string }) => {
    const [tickets, setTickets] = React.useState([]);

    React.useEffect(() => {
        fetch(`https://vietcpq.name.vn/U2FsdGVkX19OPhHMXw1V9CXSBEQUK+Pcc8fSVohfF7Y=/cinema/TicketByEmail/${mail}`)
            .then(resp => resp.json())
            .then(data => {
                setTickets(data)
            }
            );
    }, [mail])

    return (
        <div className='bought-ticket'>
            {
                tickets.map((t: any) =>
                    <div key={t.Id} className='d_flex bg-none m_2-b'>
                        <div className='boughtImg'>
                            <img alt='' src={t.ImagePortrait} />
                        </div>
                        <div className='boughtText d_flex jus_space flex_col'>
                            <h3>{t.FilmName}</h3>
                            <table className='w_100'>
                                <tbody>
                                    <tr>
                                        <th>Suất chiếu:</th>
                                        <td>{t.ShowTime.slice(11, 16)}</td>
                                    </tr>
                                    <tr>
                                        <th>Ngày chiếu:</th>
                                        <td>{t.ShowTime.slice(0, 10)}</td>
                                    </tr>
                                    <tr>
                                        <th>Rạp:</th>
                                        <td> {t.CinemaName} | {t.TheaterName}</td>
                                    </tr>
                                    <tr>
                                        <th>Combo</th>
                                        <td>{t.SeatCode}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div className='font-sm d_flex align_center'>
                                <FontAwesomeIcon icon={faTicket} className='icon-20 color_orange m_2-r' />
                                {t.ShowCode}
                            </div>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default History