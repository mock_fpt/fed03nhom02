import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

interface eUser {
    "email": string,
    "name": string,
    "password": string
}

const Register = () => {
    const [user, setUser] = useState<eUser>({ email: '', name: '', password: '' })
    const [pass2, setPass2] = useState('')
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleRegister = () => {
        if (user.email === '') {
            alert('Vui lòng không bỏ trống email')
            return
        }
        if (user.password === '' || pass2 === '') {
            alert('Vui lòng không bỏ trống password')
            return
        }
        if (pass2 !== user.password) {
            alert('Vui lòng kiểm tra lại password')
            return
        }
        dispatch({type: 'REGISTER', payload: {user, callback: navigate}})
    }

    return (
        <div className='TheRegisterForm d_flex flex-col align_center'>
            <div className='RegisterContent'>
                <h1>Đăng ký</h1>

                <div className='m_2-b'>
                    <p style={{ color: "#883000" }} className='font-weight-7'>Họ & Tên</p>
                    <input
                        value={user.name}
                        onChange={(e) => setUser({...user, name: e.target.value})}
                    />
                </div>

                <div className='m_2-b'>
                    <p style={{ color: "#883000" }} className='font-weight-7'>Email</p>
                    <input
                        value={user.email}
                        onChange={(e) => setUser({...user, email: e.target.value})}
                        type={'email'}
                    />
                </div>

                <div className='m_2-b'>
                    <p style={{ color: "#883000" }} className='font-weight-7'>Mật khẩu</p>
                    <input
                        value={user.password}
                        onChange={(e) => setUser({...user, password: e.target.value})}
                        type={'password'}
                    />
                </div>

                <div className='p_2-b'>
                    <p style={{ color: "#883000" }} className='font-weight-7'>Nhập lại mật khẩu</p>
                    <input
                        value={pass2}
                        onChange={(e) => setPass2(e.target.value)}
                        type={'password'}
                    />
                </div>

                <button className='w_100 m_2-t rounded-sm font-weight-7' onClick={handleRegister}>
                    Đăng ký
                </button>
            </div>
        </div>
    )
}

export default Register