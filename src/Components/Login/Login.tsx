import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import './Login.scss'
import Register from './Register'

export default function Login() {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [box, setBox] = useState(true)
    const [user, setUser] = useState(true)
    const [pass, setPass] = useState(true)
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const HandleLogin = () => {
        setBox(true)
    }

    const loginEvent = () => {
        if (username !== '' && password !== '') {
            dispatch({ type: 'LOGIN', payload: { username, password } })
            navigate('/')
        }
        else alert('Vui lòng không bỏ trống email hoặc password!')
    }

    const HandleRegister = () => {
        setBox(false)
    }

    const ChoseUser = () => {
        setUser(false)
        setPass(true)
    }

    const ChosePass = () => {
        setPass(false)
        setUser(true)
    }

    return (
        <div className='MainLogin'>
            <div className='Behide_Log'>
                <div className='LoginForm'>
                    <div className='LoginForm_BTN'>
                        <div className='BackgrondBtn' style={{ right: box ? "50%" : "0", }} ></div>
                        <button className='font-weight-7' onClick={HandleLogin}>Login</button>
                        <button className='font-weight-7' onClick={HandleRegister}>Register</button>
                    </div>
                    {
                        box && <div className='TheLoginForm d_flex flex_col align_center'>
                            <div className='LoginContent' >
                                <h1>Đăng nhập</h1>
                                <div className='font-sm text-center'>
                                    <p>
                                        Test accout: user2@gmail.com <br></br>
                                        Password: 222
                                    </p>
                                    <p>Note: xin đừng đổi password</p>
                                </div>
                                <div className='m_2-b'>
                                    <p className='font-weight-7' style={{ color: user ? "#B1560F" : "#883000" }}>Email</p>
                                    <input
                                        value={username}
                                        onChange={(e) => setUsername(e.target.value)}
                                        style={{ backgroundColor: user ? "transparent" : "#FFD73B" }}
                                        onFocus={ChoseUser}
                                    />
                                </div>
                                <div className='m_2-b'>
                                    <p className='font-weight-7' style={{ color: pass ? "#B1560F" : "#883000" }}>Mật khẩu</p>
                                    <input
                                        style={{ backgroundColor: pass ? "transparent" : "#FFD73B" }}
                                        onFocus={ChosePass} type={'password'}
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                                </div>

                                <button style={{ marginTop: '10px' }}
                                    className='my_2 w_100 rounded-sm font-weight-7'
                                    onClick={loginEvent}
                                    type='submit'
                                >
                                    Đăng nhập
                                </button>
                            </div>
                        </div>
                    }
                    {
                        !box && <Register />
                    }
                </div>
            </div>
        </div>
    )
}
