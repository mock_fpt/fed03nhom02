import './PageNotFound.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleQuestion } from '@fortawesome/free-regular-svg-icons'
import { Link } from 'react-router-dom'


export default function PageNotFound() {
    return (
        <div className="m_auto main_width" id='pageNotFound'>
            <div className="err text-center py_5">
                <h2>
                    <span style={{color:'darkorange'}}>4</span>
                    <FontAwesomeIcon icon={faCircleQuestion} className='fa-spin' style={{color:'sandybrown'}}/>
                    <span style={{color:'orange'}}>4</span>
                </h2>
                <div className='py_3'> 
                    {/* <h3 className='font-weight-2'>Page does not exist. Click here to go back to the homepage.</h3> */}
                    <h3 className='font-weight-2'>Trang không tồn tại. Nhấn vào <Link to={'/'}>đây</Link> để về lại trang chủ.</h3>
                </div>
            </div>
        </div>
    )
}
