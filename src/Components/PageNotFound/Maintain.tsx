import { faCircleQuestion } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './PageNotFound.scss'

const Maintain = () => {
    return (
        <div id='server-error'>
            <div className='py_5 text-center m_auto main_width'>
                <h1>
                    5
                    <FontAwesomeIcon icon={faCircleQuestion} className='fa-spin' />
                    0
                </h1>
                <h2>Server Error</h2>
                <h3>
                    Hiện tại server đang bảo trì. Quý khách có thể thử quay lại sau.
                    <br></br>
                    Chúng tôi thành thật xin lỗi vì sự bất tiện này.
                </h3>
            </div>
        </div>
    )
}

export default Maintain