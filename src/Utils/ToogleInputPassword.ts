export const showPassword = (name: string) => {
    const inputElement = document.querySelector(`input[name=${name}]`) as HTMLInputElement;
    inputElement.type = inputElement.type === 'text' ? 'password' : 'text';
}